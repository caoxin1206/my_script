#!/bin/sh

sdir=$(dirname $0);

if [ $# -ne 1 ]
then
    echo para num error!
    exit -1
fi
rootfs=$1
if `! ls $rootfs > /dev/null`
then
    echo $rootfs is not dir!
    exit -1
fi

# 拷贝服务器密匙
sudo chmod 600 $sdir/keys/ssh*
cp $sdir/keys/ssh* $rootfs/etc/ssh/

# 添加sshd启动脚本
chmod +x -R $sdir/scripts/
cp $sdir/scripts/* $rootfs/etc/init.d/

# 添加客户端公匙
mkdir $rootfs/root/.ssh/
cp $sdir/auth/authorized_keys $rootfs/root/.ssh/

# 修改sshd配置
mv $rootfs/etc/ssh/sshd_config $rootfs/etc/ssh/sshd_config-b
cp $sdir/sshd_config $rootfs/etc/ssh/
