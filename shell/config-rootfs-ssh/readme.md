# config steps

- /etc/password add "sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin"
- set root user password: password root
- careate keys:
```
cd /usr/local/etc/
ssh-keygen -t rsa -f ssh_host_rsa_key -N ""
ssh-keygen -t dsa -f ssh_host_dsa_key -N ""
ssh-keygen -t ecdsa -f ssh_host_ecdsa_key -N ""
ssh-keygen -t dsa -f ssh_host_ed25519_key -N ""
```
- mount devpts:
```
mkdir /dev/pts
mount devpts /dev/pts -t devpts
```
- add start scripts:
```
# config net :/etc/init.d/S41network
#!/bin/sh
echo " --- ifconfig eth0 up --- "
ifconfig eth0 up
sleep 1
s=0
while [ $s -eq 0 ]
do
s=$(cat /sys/class/net/eth0/carrier)
sleep 1
done
#ifconfig eth0 up
#sleep 1
udhcpc -i eth0 -t 3 -n
#ntp
sleep 5
ntpdate -u ntp.api.bz #上海ntp 服务器


# mount pts and start:/etc/init.d/S49sshd
mkdir /dev/pts
mount devpts /dev/pts -t devpts
echo "mount devpts.."
/usr/sbin/sshd
```