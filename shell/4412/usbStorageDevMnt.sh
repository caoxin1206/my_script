#!/bin/sh

if [ $# -ne 1 ]
then
    echo para num error!
    exit -1
fi

if ! [ -b $1 ]
then
    echo block file not exist!
    exit -1;
fi

echo mnt ...!
modprobe libcomposite.ko
modprobe usb_f_mass_storage.ko
modprobe g_mass_storage.ko file=$1 removable=1