#!/bin/sh

DRIVER_PATH=/lib/modules/$(uname -r)/8188eu.ko
SSID=HLK_WIFI6_374e
PASSWORD=cx12345.

# step 1: install module
lsmod | grep 8188 || insmod $DRIVER_PATH
if [ 1 = $? ]                    
then
  echo 'insmod failed!'
  return -1;   
fi
sleep 1

# step2: generate conf
if [ ! -f /etc/wpa_supplicant.conf ];then
  wpa_passphrase $SSID $PASSWORD > /etc/wpa_supplicant.conf
  echo 'generate password file!'
fi

# step3: pkill old process
ps | grep wpa_supplicant > /dev/null
if [ 0 = $? ]           
then                    
  pkill wpa_supplicant
  sleep 1
  echo 'kill wpa_supplicant'
fi

ps | grep udhcpc > /dev/null
if [ 0 = $? ]                                                      
then                                                               
  pkill udhcpc 
  echo 'pkill udhcpc'                                              
  sleep 1                                                                       
fi

# step4: configure network interface
wpa_supplicant  -B -i wlan0 -D wext -c /etc/wpa_supplicant.conf > /dev/null
if [ 1 = $? ]
then
  echo 'wpa_supplicant failed!'
  return -1;
fi
sleep 3

udhcpc -i wlan0
ifconfig wlan0 192.168.16.130
