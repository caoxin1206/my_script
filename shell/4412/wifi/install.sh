#!/bin/sh

rm /etc/wpa_supplicant.conf


if ! [ -e /lib/modules/$(uname -r) ]
then
  mkdir -p /lib/modules/$(uname -r)
fi

cp 8188eu.ko /lib/modules/$(uname -r)
mkdir -p /lib/firmware/rtlwifi
cp rtl8188eufw.bin /lib/firmware/rtlwifi