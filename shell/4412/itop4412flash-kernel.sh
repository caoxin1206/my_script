#!/bin/sh

if [ $# -ne 1 ]
then
    echo para num error!
    exit -1
fi

if ! [ -e $1 ]
then
    echo file not exist!
    exit -1;
fi

echo -n Is flash kernel: $1 ?
read buf

#	"mmc read ${loadaddr} 0x460 0x3000; mmc read ${dtb_addr} 0x3460 0xa0; bootm ${loadaddr} - ${dtb_addr}" \

dd if=$1 of=/dev/mmcblk1 conv=sync seek=1120