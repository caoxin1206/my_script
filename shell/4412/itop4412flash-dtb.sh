#!/bin/sh

if [ $# -ne 1 ]
then
    echo para num error!
    exit -1
fi

if ! [ -e $1 ]
then
    echo file not exist!
    exit -1;
fi

echo -n Is flash dtb: $1 ?
read buf


dd if=$1 of=/dev/mmcblk1 conv=sync seek=13408