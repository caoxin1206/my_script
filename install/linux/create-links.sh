#!/bin/sh

cd ../../../links-target/
target_dir=$(pwd)
cd ../../
obj_dir=$(pwd)

list=$(ls -a $target_dir)

for file in $list
do
	if [ $file != '.' -a $file != '..' ]
	then
		ln -s $target_dir/$file $obj_dir/$file
	fi
done

