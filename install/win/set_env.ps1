# 其中GetEnvironmentVariable，一个参数是变量名，第二个参数target是范围，可以是 
# Process（当前进程），User（当前用户）或 Machine（本地计算机）


# 获取当前的 path 值
$path = [System.Environment]::GetEnvironmentVariable("Path","Machine")
# 添加新的路径到 path 中
$path += ';"D:\01-BackDir\06-tools\BCompare Pro 4.4.0.25886 x64"'
# 设置新的 path 值
[System.Environment]::SetEnvironmentVariable("Path",$path,"User")
