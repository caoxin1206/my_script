#!/bin/python3

print("hello world!")

from http.server import HTTPServer, SimpleHTTPRequestHandler

def run(server_address):
    httpd = HTTPServer(server_address, SimpleHTTPRequestHandler)
    print("HTTP server running on port 8000")
    httpd.serve_forever()

if __name__ == "__main__":
    run(('', 8000))  # Serve on all interfaces and port 8000


