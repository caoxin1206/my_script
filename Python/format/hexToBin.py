# 将16进制文本转换为bin文件
import sys
 
def count_args():
    # 返回命令行参数的数量，不包括脚本名
    return len(sys.argv) - 1
 
print(f"输入参数的数量是: {count_args()}")
if count_args() == 1 :
    print("using extern text")
    input_text = sys.argv[1]
elif count_args() == 0:
    print("using default text")
    default_text = "7EB44692E57CF0C2C2DD22ED2D5D349303D10ABAC46D1977A91907DF3ABD12B0"
    input_text = default_text
else:
    print("input arg num err")
    exit(-1)

if len(input_text) % 2 != 0 :
    print("input_text format error!")
    exit(-1)


bin_array=[]
for i in range(0, len(input_text), 2):
    bin_array.append(int(input_text[i:i+2], 16))

#print(bin_array)

file = open("output-bin", "+wb")
# for i in range(0, len(bin_array)):
file.write(bytes(bin_array))

file.close()
