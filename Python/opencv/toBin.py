import cv2
import copy
import numpy as np

img = cv2.imread('image/barcode.jpg')
# 转为灰度图
gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
raw_gray_img = copy.deepcopy(gray_img)
height, width = gray_img.shape[0:2]
print("height:" + str(height) + ",width:" + str(width))


# 设置阈值
thresh = 60

# 遍历每一个像素点
for row in range(height):
    for col in range(width):
        # 获取到灰度值
        gray = gray_img[row, col]
        # 如果灰度值高于阈值 就等于255最大值
        if gray > thresh:
            gray_img[row, col] = 255
        # 如果小于阈值，就直接改为0
        elif gray < thresh:
            gray_img[row, col] = 0

# 中值滤波
# median_filtered = cv2.medianBlur(gray_img, 5)  # 5是滤波核的大小，可以根据需要调整

blurred = cv2.bilateralFilter(gray_img, 9, 75, 75)
kernel_sharpening = np.array([[-1,-1,-1], 
                              [-1, 9,-1], 
                              [-1,-1,-1]])
sharpened = cv2.filter2D(blurred, -1, kernel_sharpening)
gray_img = sharpened


cv2.imshow('img', gray_img)
# cv2.imshow('img2', raw_gray_img)
cv2.imwrite('outbinary_image.png', gray_img)
cv2.waitKey()
