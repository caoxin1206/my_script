import os
import sys
import datetime
import re
import time;

def isAscii(ch) :
    return ch <= u'\u007f'


def formatByWidth(text, width, align_left=True) :
    pad = " " * (width - strlen(text))
    if align_left :
        return text + pad
    else :
        return pad + text


def strlen(str) :
    count = len(str)
    for u in str:
        if not isAscii(u) :
            count += 1
    return count


#return directory total size, unit K
def getDirSize(dir) :
    size = 0;
    try:
        if os.path.isdir(dir) :
            for file in os.listdir(dir) :
                path = os.path.join(dir, file)
                if os.path.isdir(path) :
                    size += getDirSize(path)
                elif os.path.isfile(path) :
                    size += os.path.getsize(path)
                else : 
                    print(file)
    except IOError :
        pass
    finally:
        return size;


def getTime(file) :
    timestamp = os.path.getmtime(file)
    return datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')


def formatNum(num):
    num=str(num)
    pattern=r'(\d+)(\d{3})'
    while True:
        num,count=re.subn(pattern,r'\1,\2',num)
        if count==0:
            break
    return num


def ls(dir) :
    # dir = dir.decode('utf-8')
    maxLength = 0
    for file in os.listdir(dir) :
        filenameLen = strlen(file)
        if filenameLen > maxLength :
            maxLength = filenameLen;

    totalSize = 0
    filenameWidth = maxLength
    timeWidth = 19
    sizeWidth = 15
    print("%s  %s  %s" % (formatByWidth("文件名", filenameWidth, False), \
    formatByWidth("最后修改日期", timeWidth), \
    formatByWidth("大小(字节)", sizeWidth, False)))
    for file in os.listdir(dir) :
        path = os.path.join(dir, file)
        if os.path.isdir(path) :
            size = getDirSize(path)
        else : 
            size = os.path.getsize(path)
        totalSize += size
        print("%s  %s  %s" % (formatByWidth(file, filenameWidth, False), \
        formatByWidth(getTime(path), timeWidth), \
        formatByWidth(formatNum(size), sizeWidth, False)))

    print("目录总大小：%s（字节）" % formatNum(totalSize))
    print("目录总大小：%.2f（KB）" % (totalSize/(1024)))
    print("目录总大小：%.2f（MB）" % (totalSize/(1024*1024)))
    print("目录总大小：%.2f（GB）" % (totalSize/(1024*1024*1024)))

if len(sys.argv) > 1 :
    ls(sys.argv[1])
else :
    ls('.\\')

input("输入回车退出")
    